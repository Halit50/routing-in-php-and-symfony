<?php 

use Symfony\Component\Config\FileLocator;
use App\Loader\CustomAnnotationClassLoader;
use Symfony\Component\Routing\RequestContext;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Loader\AnnotationDirectoryLoader;

require __DIR__.'/../vendor/autoload.php';


// Le premier paramètre est la route , le 2ème en forme de tableau est une valeur par défaut qu'on peut donner
// transféré dans le config/routes.php
//$helloRoute = new Route('/hello/{name}', ['name' => 'World' , 'controller' => 'App\Controller\HelloController@sayHello']);

//$listRoute = new Route('/', ['controller' => 'App\Controller\TaskController@index']);
// Le 5ème paramètre permet de définir un host sur lequel la route sera disponible, le 6ème on peut préciser si la route sera dispo sur http ou https ou les et le 7ème paramètre concerne la méthode d'accès à la route soit en GET soit en POST
// !! le host localhost est à utiliser seulement si on est en locale, en prod il faut donner le nom de domaine
//$createRoute = new Route('/create', ['controller' => 'App\Controller\TaskController@create'], [], [],'localhost', ['http', 'https'], ['GET', 'POST']);
// nous pouvons ajouter des requirements en 3ème paramètre de l'instanciation de la classe Route comme par exemple des regex qui ici signifie que l'id doit etre un numérique et posséder au moins 1 caractère.
// Le ? derrière le paramère id signifie qu'il n'est pas obligatoire de renseigner un id et que la route show sera accessible sans renseigner d'id 
// Après le ?, on peut renseigner un id qui sera utilisé par défaut si aucun id est renseigné lorsque qu'on utilise la route
// $showRoute = new Route('/show/{id?100}', ['controller' => 'App\Controller\TaskController@show'], [
//     'id' => '\d+'
// ]);

//**************************** Utilisation du loader via un fichier PHP *****************/
//$loader = new PhpFileLoader(new FileLocator(__DIR__.'/config'));
//$collection = $loader->load('routes.php');

//**************************** Utilisation du loader via un fichier YAML *****************/
// $loader = new YamlFileLoader(new FileLocator(__DIR__.'/config'));
// $collection = $loader->load('routes.yaml');

//**************************** Plus besoin de faire ce traitement *****************/
// $classLoader = require __DIR__.'/vendor/autoload.php';
// AnnotationRegistry::registerLoader([$classLoader, 'loadClass']);

//**************************** Utilisation du loader via les Annotations *****************/
$loader = new AnnotationDirectoryLoader(
    new FileLocator(__DIR__.'/../src/Controller'),
    new CustomAnnotationClassLoader(new AnnotationReader())
);

$collection = $loader->load(__DIR__.'/../src/Controller');


// $collectionRoute = new RouteCollection();
// $collectionRoute->add('list', $listRoute);
// $collectionRoute->add('create', $createRoute);
// $collectionRoute->add('show', $showRoute);
// $collectionRoute->add('hello', $helloRoute);


// nous pouvons renseigner dans le RequestContext de se sevir de la méthode de la variable globale $_SERVER pour déterminer de quel méthode est accéder à la route (valable egalement pour le host ou les scheme (http, https))
$matcher = new UrlMatcher($collection, new RequestContext('', $_SERVER['REQUEST_METHOD']));
$generator = new UrlGenerator($collection, new RequestContext());

$pathInfo = $_SERVER['PATH_INFO'] ?? '/';