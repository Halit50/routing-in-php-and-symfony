<?php

use Symfony\Component\Routing\Exception\ResourceNotFoundException;

require __DIR__.'/config/bootstrap.php';

try {

    $currentRoute = $matcher->match($pathInfo);
    dump($currentRoute);

    $currentRoute['generator'] = $generator;
    $controller = $currentRoute['_controller'];
    $className = substr($controller,0, strpos($controller,'@'));
    $methodName = substr($controller, strpos($controller,'@') +1);
    $instance = new $className();

    call_user_func([$instance, $methodName], $currentRoute);

} catch (ResourceNotFoundException $e){
   
    require_once "pages/404.html.php";
    return;
}